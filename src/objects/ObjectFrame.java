package objects;

import com.sun.deploy.util.ArrayUtil;
import org.opencv.core.*;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.CascadeClassifier;

import javax.swing.*;
import java.awt.*;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class ObjectFrame extends JFrame {
    private JPanel cameraPanel;
    private JPanel northButtonPanel;
    private JLabel cameraLabel;
    private JComboBox<Classifier> classifierJComboBox;
    private Mat frame;
    private Mat imageFilter;
    private CascadeClassifier cascadeClassifier;
    private Mat grayscaleImage = new Mat(480, 640, CvType.CV_8UC4);
    private int absoluteFaceSize = (int) (480 * 0.2);

    private ArrayList<Classifier> classifiers;

    public ObjectFrame() {

        this.classifiers = new ArrayList<>();

        this.classifiers.add( new Classifier(new CascadeClassifier( new File("res", "lbpcascade_silverware.xml").getAbsolutePath()), "silverware" ));
        this.classifiers.add(new Classifier(new CascadeClassifier( new File("res", "Hand.Cascade.xml").getAbsolutePath()), "hand" ));
        this.classifiers.add(new Classifier(new CascadeClassifier( new File("res", "car.xml").getAbsolutePath()), "car" ));
        this.classifiers.add(new Classifier(new CascadeClassifier( new File("res", "WallClock.xml").getAbsolutePath()), "clock" ));
        this.classifiers.add(new Classifier(new CascadeClassifier( new File("res", "haarcascade_eye.xml").getAbsolutePath()), "eye" ));
        this.classifiers.add(new Classifier(new CascadeClassifier( new File("res", "haarcascade_frontalcatface_extended.xml").getAbsolutePath()), "cat" ));
        this.classifiers.add(new Classifier(new CascadeClassifier( new File("res", "haarcascade_fullbody.xml").getAbsolutePath()), "human" ));



        initializeOpenCVDependencies();
        setTitle("Object Detection");
        makeComponents();
        setLayout();
        addListeners();
        showFrame();
    }

    private void makeComponents() {

        cameraPanel = new JPanel();
        northButtonPanel = new JPanel();
        cameraLabel = new JLabel();
        classifierJComboBox = new JComboBox(classifiers.toArray());

        imageFilter = Imgcodecs.imread("res/floatinghearth.png");
    }

    private void setLayout() {
        cameraPanel.setSize(400, 400);
        northButtonPanel.setLayout(new FlowLayout());
        northButtonPanel.add(classifierJComboBox);
        cameraPanel.add(cameraLabel);

        setLayout(new BorderLayout());
        add(cameraPanel, BorderLayout.CENTER);

        add(northButtonPanel, BorderLayout.NORTH);
    }

    private void showFrame() {
        setVisible(true);
        setSize(650, 650);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setLocation(getWidth() / 2, getHeight() / 11);
    }

    public void updateCamera(Mat frame) {

        // get the persons face location.





        Imgproc.cvtColor(frame, grayscaleImage, Imgproc.COLOR_RGBA2RGB);

       // MatOfRect faces = new MatOfRect();

        frame = CascadeDetector(frame, classifiers.get(classifierJComboBox.getSelectedIndex()));









        this.frame = frame;
        cameraLabel.setIcon(new ImageIcon(convertMatBufImg(frame)));
    }


    private Mat CascadeDetector(Mat frame, Classifier classifier ){

            MatOfRect detectedObjects = new MatOfRect();
            classifier.getCascadeClassifier().detectMultiScale(grayscaleImage, detectedObjects, 1.1, 2, 2,
                    new Size(frame.height()*0.2, frame.width()*0.2), new Size());

            final Rect[] detectedArraysObjects = detectedObjects.toArray();
            for (int i = 0; i < detectedArraysObjects.length; i++) {
                Imgproc.rectangle(frame, detectedArraysObjects[i].tl(), detectedArraysObjects[i].br(), new Scalar(0, 255, 0, 255), 3);
                Imgproc.putText(frame, classifier.getName(), detectedArraysObjects[i].tl(), 2, 1.5, new Scalar(0, 255, 0, 255) );
            }



        return frame;
    }


    private void addListeners() {

    }


    private void initializeOpenCVDependencies() {
        try {
            File mCascadeFile = new File("res", "haarcascade_frontalface_default.xml");

            // Load the cascade classifier
            cascadeClassifier = new CascadeClassifier(mCascadeFile.getAbsolutePath());

        } catch (Exception e) {
            System.out.println("OpenCVActivity Error loading cascade " + e.getMessage());
        }
    }


    public static void overlayImage(Mat background, Mat foreground, Mat output, Point location) {
        // Imgproc.cvtColor(background, background, CvType.CV_8UC1);
        // Imgproc.cvtColor(foreground, foreground, CvType.CV_8UC1);
        background.copyTo(output);

        for (int y = (int) Math.max(location.y, 0); y < background.rows(); ++y) {

            int fY = (int) (y - location.y);

            if (fY >= foreground.rows())
                break;

            for (int x = (int) Math.max(location.x, 0); x < background.cols(); ++x) {
                int fX = (int) (x - location.x);
                if (fX >= foreground.cols()) {
                    break;
                }

                double opacity;
                double[] finalPixelValue = new double[3];

                //  opacity = foreground.get(fY , fX)[3];

                finalPixelValue[0] = background.get(y, x)[0];
                finalPixelValue[1] = background.get(y, x)[1];
                finalPixelValue[2] = background.get(y, x)[2];
                //finalPixelValue[3] = background.get(y, x)[3];

                for (int c = 0; c < output.channels(); ++c) {

                    double foregroundPx = foreground.get(fY, fX)[c];
                    double backgroundPx = background.get(y, x)[c];


                    finalPixelValue[c] = (backgroundPx) + (foregroundPx);
                    if (c == 3) {
                        finalPixelValue[c] = foreground.get(fY, fX)[3];
                    }

                }
                output.put(y, x, finalPixelValue);
            }
        }
    }

    public Mat removeBlackTransparency(Mat src) {
        Mat dst = new Mat();//(src.rows,src.cols,CV_8UC4);
        Mat tmp = new Mat();
        Mat alpha = new Mat();

        Imgproc.cvtColor(src, tmp, Imgproc.COLOR_BGR2GRAY);
        Imgproc.threshold(tmp, alpha, 100, 255, Imgproc.THRESH_BINARY);

        List<Mat> rgb = new ArrayList<>(); // rgb[3];
        Core.split(src, rgb);

        List<Mat> rgba = new ArrayList<>();
        rgba.add(rgb.get(0));
        rgba.add(rgb.get(1));
        rgba.add(rgb.get(2));
        rgba.add(alpha);
        Core.merge(rgba, dst); //4 ?
        return dst;
    }

    private static BufferedImage matToBufferedImage(Mat frame) {
        int type = 0;
        if (frame.channels() == 1) {
            type = BufferedImage.TYPE_BYTE_GRAY;
        } else if (frame.channels() == 3) {
            type = BufferedImage.TYPE_3BYTE_BGR;
        }
        BufferedImage image = new BufferedImage(frame.width(), frame.height(), type);
        WritableRaster raster = image.getRaster();
        DataBufferByte dataBuffer = (DataBufferByte) raster.getDataBuffer();
        byte[] data = dataBuffer.getData();
        frame.get(0, 0, data);

        return image;
    }

    private static BufferedImage convertMatBufImg(Mat frame) {
        //Imgproc.cvtColor(frame, frame, Imgproc.COLOR_RGB2GRAY, 0);

        // Create an empty image in matching format
        BufferedImage gray = new BufferedImage(frame.width(), frame.height(), BufferedImage.TYPE_3BYTE_BGR);

        // Get the BufferedImage's backing array and copy the pixels directly into it
        byte[] data = ((DataBufferByte) gray.getRaster().getDataBuffer()).getData();
        frame.get(0, 0, data);
        return gray;
    }

    public class Classifier{
        private CascadeClassifier cascadeClassifier;
        private String name;

        public Classifier(CascadeClassifier cascadeClassifier, String name) {
            this.cascadeClassifier = cascadeClassifier;
            this.name = name;
        }

        public CascadeClassifier getCascadeClassifier() {
            return cascadeClassifier;
        }

        public void setCascadeClassifier(CascadeClassifier cascadeClassifier) {
            this.cascadeClassifier = cascadeClassifier;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        @Override
        public String toString() {
            return "Classifier{" +
                    name +
                    '}';
        }
    }


}

