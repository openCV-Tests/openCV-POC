package objects;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.videoio.VideoCapture;

/**
 * Created by Fl4il on 17/02/2018.
 */
public class ObjectMain {

    public static void main(String[] args) {


        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);

        VideoCapture camera = new VideoCapture(0);

        if (!camera.isOpened()) {
            System.out.println("Error! Camera can't be opened!");
            return;
        }
        Mat frame = new Mat();
        ObjectFrame objectFrame = new ObjectFrame();

        while (true) {

            while (true) {
                if (camera.read(frame)) {
                    /*System.out.println("Frame Obtained");
                    System.out.println("Captured Frame Width " +
                            frame.width() + " Height " + frame.height());
                    System.out.println("OK");*/
                    break;
                }
            }

            //BufferedImage bufferedImage = matToBufferedImage(frame);
            objectFrame.updateCamera(frame);
            /*
            double fps = camera.get(Videoio.CAP_PROP_FPS);
            System.out.println(fps);*/ //works only on videofiles
        }
    }
}
