import org.opencv.core.*;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.CascadeClassifier;

import javax.swing.*;
import java.awt.*;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class mainFrame extends JFrame {
    private boolean exit = false;
    private long t1,t2;
    private JButton btnTakePicture;
    private JButton btnSwitchFilter;
    private JButton btnExit;
    private JLabel lblFps;
    private JPanel cameraPanel;
    private JPanel northButtonPanel;
    private JLabel cameraLabel;
    private Mat frame;
    private Mat imageFilter;
    private CascadeClassifier cascadeClassifier;
    private Mat grayscaleImage = new Mat(480, 640, CvType.CV_8UC4);
    private int absoluteFaceSize = (int) (480 * 0.2);

    public mainFrame() {
        initializeOpenCVDependencies();
        setTitle("OpenCV");
        makeComponents();
        setLayout();
        addListeners();
        showFrame();

        t1= System.currentTimeMillis();
        t2 = System.currentTimeMillis();
    }

    private void makeComponents() {
        btnTakePicture = new JButton("Take picture");
        btnSwitchFilter = new JButton("Switch filter");
        btnExit = new JButton("Exit");
        cameraPanel = new JPanel();
        northButtonPanel = new JPanel();
        cameraLabel = new JLabel();
        lblFps = new JLabel("0 FPS");
        imageFilter = Imgcodecs.imread("res/floatinghearth.png");
    }

    private void setLayout() {
        cameraPanel.setSize(400, 400);
        northButtonPanel.setLayout(new FlowLayout());
        northButtonPanel.add(btnTakePicture);
        northButtonPanel.add(btnSwitchFilter);
        northButtonPanel.add(btnExit);
        northButtonPanel.add(lblFps);
        cameraPanel.add(cameraLabel);

        setLayout(new BorderLayout());
        add(cameraPanel, BorderLayout.CENTER);
        add(northButtonPanel, BorderLayout.NORTH);
    }

    private void showFrame() {
        setVisible(true);
        setSize(650, 650);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setLocation(getWidth() / 2, getHeight() / 11);
    }

    public void updateCamera(Mat frame) {
        double fps = calculateFps();
        lblFps.setText(String.format("%6.2f FPS",fps));

        // get the persons face location.

        Mat out = new Mat();

        // Imgproc.cvtColor(frame, out, Imgproc.COLOR_BGR2GRAY);
        //imageFilter = removeBlackTransparency(imageFilter);
        //Imgproc.cvtColor(frame,frame,Imgproc); //bgr back to rgb before saving image


        /*Mat n = frame.colRange(10, 500).rowRange(10, 300).setTo(new Scalar(224, 224, 224));
        n.copyTo(frame.submat(10, 300, 10, 500));
        Rect roi = new Rect(10, 10,  n.cols(),n.rows());
        Core.addWeighted(frame.submat(roi), 0.8, n, 0.2, 1,  frame.submat(roi));*/

        Imgproc.cvtColor(frame, grayscaleImage, Imgproc.COLOR_RGBA2RGB);

        MatOfRect faces = new MatOfRect();

        if (cascadeClassifier != null) {
            cascadeClassifier.detectMultiScale(grayscaleImage, faces, 1.1, 2, 2,
                    new Size(absoluteFaceSize, absoluteFaceSize), new Size());
        }

        final Rect[] facesArray = faces.toArray();
        for (int i = 0; i < facesArray.length; i++) {
            Imgproc.rectangle(frame, facesArray[i].tl(), facesArray[i].br(), new Scalar(0, 255, 0, 255), 3);
        }

        Point p = new Point();
        out = frame;
        boolean facefound = false;
        for (int i = 0; i < facesArray.length; i++) {
            facefound = true;
            p = new Point(Integer.parseInt(String.valueOf(Math.round(facesArray[i].tl().x -100))), Integer.parseInt(String.valueOf(Math.round(facesArray[i].tl().y -100)))); //afgesteld om filter op correcte plaats te krijgen
            // overlayImage(inputFrame,filterImage,out,p);
            overlayImage(out, imageFilter, out, p);

        }




        this.frame = frame;
        cameraLabel.setIcon(new ImageIcon(convertMatBufImg(out)));
    }

    private double calculateFps() {
        t2 = System.currentTimeMillis();
        long delta = t2-t1;
        t1 = t2;
        if (delta == 0){
            return Double.MAX_VALUE;
        }
        return 1000.0/(double) delta;
    }

    private void addListeners() {
        btnTakePicture.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Imgcodecs.imwrite("camera.jpg", frame);
            }
        });
        btnSwitchFilter.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

            }
        });
        btnExit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                exit=true;
            }
        });
    }


    private void initializeOpenCVDependencies() {
        try {
            File mCascadeFile = new File("res", "haarcascade_frontalface_default.xml");

            // Load the cascade classifier
            cascadeClassifier = new CascadeClassifier(mCascadeFile.getAbsolutePath());

        } catch (Exception e) {
            System.out.println("OpenCVActivity Error loading cascade " + e.getMessage());
        }
    }


    public boolean isExit() {
        return exit;
    }

    public static void overlayImage(Mat background, Mat foreground, Mat output, Point location) {
        // Imgproc.cvtColor(background, background, CvType.CV_8UC1);
        // Imgproc.cvtColor(foreground, foreground, CvType.CV_8UC1);
        background.copyTo(output);

        for (int y = (int) Math.max(location.y, 0); y < background.rows(); ++y) {

            int fY = (int) (y - location.y);

            if (fY >= foreground.rows())
                break;

            for (int x = (int) Math.max(location.x, 0); x < background.cols(); ++x) {
                int fX = (int) (x - location.x);
                if (fX >= foreground.cols()) {
                    break;
                }

                double opacity;
                double[] finalPixelValue = new double[3];

                //  opacity = foreground.get(fY , fX)[3];

                finalPixelValue[0] = background.get(y, x)[0];
                finalPixelValue[1] = background.get(y, x)[1];
                finalPixelValue[2] = background.get(y, x)[2];
                //finalPixelValue[3] = background.get(y, x)[3];

                for (int c = 0; c < output.channels(); ++c) {

                    double foregroundPx = foreground.get(fY, fX)[c];
                    double backgroundPx = background.get(y, x)[c];


                    finalPixelValue[c] = (backgroundPx) + (foregroundPx);
                    if (c == 3) {
                        finalPixelValue[c] = foreground.get(fY, fX)[3];
                    }

                }
                output.put(y, x, finalPixelValue);
            }
        }
    }

    public Mat removeBlackTransparency(Mat src) {
        Mat dst = new Mat();//(src.rows,src.cols,CV_8UC4);
        Mat tmp = new Mat();
        Mat alpha = new Mat();

        Imgproc.cvtColor(src, tmp, Imgproc.COLOR_BGR2GRAY);
        Imgproc.threshold(tmp, alpha, 100, 255, Imgproc.THRESH_BINARY);

        List<Mat> rgb = new ArrayList<>(); // rgb[3];
        Core.split(src, rgb);

        List<Mat> rgba = new ArrayList<>();
        rgba.add(rgb.get(0));
        rgba.add(rgb.get(1));
        rgba.add(rgb.get(2));
        rgba.add(alpha);
        Core.merge(rgba, dst); //4 ?
        return dst;
    }

    private static BufferedImage matToBufferedImage(Mat frame) {
        int type = 0;
        if (frame.channels() == 1) {
            type = BufferedImage.TYPE_BYTE_GRAY;
        } else if (frame.channels() == 3) {
            type = BufferedImage.TYPE_3BYTE_BGR;
        }
        BufferedImage image = new BufferedImage(frame.width(), frame.height(), type);
        WritableRaster raster = image.getRaster();
        DataBufferByte dataBuffer = (DataBufferByte) raster.getDataBuffer();
        byte[] data = dataBuffer.getData();
        frame.get(0, 0, data);

        return image;
    }

    private static BufferedImage convertMatBufImg(Mat frame) {
        //Imgproc.cvtColor(frame, frame, Imgproc.COLOR_RGB2GRAY, 0);

        // Create an empty image in matching format
        BufferedImage gray = new BufferedImage(frame.width(), frame.height(), BufferedImage.TYPE_3BYTE_BGR);

        // Get the BufferedImage's backing array and copy the pixels directly into it
        byte[] data = ((DataBufferByte) gray.getRaster().getDataBuffer()).getData();
        frame.get(0, 0, data);
        return gray;
    }

    public void readyToExit() {
        //main loop is done, exit the program
        dispose();
    }
}
